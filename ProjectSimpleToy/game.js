/*
game.js for Perlenspiel 3.3.x
Last revision: 2018-10-14 (BM)

Perlenspiel is a scheme by Professor Moriarty (bmoriarty@wpi.edu).
This version of Perlenspiel (3.3.x) is hosted at <https://ps3.perlenspiel.net>
Perlenspiel is Copyright © 2009-18 Worcester Polytechnic Institute.
This file is part of the standard Perlenspiel 3.3.x devkit distribution.

Perlenspiel is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Perlenspiel is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You may have received a copy of the GNU Lesser General Public License
along with the Perlenspiel devkit. If not, see <http://www.gnu.org/licenses/>.
*/

/* jshint browser : true, devel : true, esversion : 5, freeze : true */
/* globals PS : true */

var COLOR_BLANK = 0xF2E9E1;
var COLOR_WALL = 0x1C140D;
var COLOR_SNAKE = 0xCBE86B;

var snake;

PS.init = function (system, options) {
	"use strict";

	PS.gridSize(17, 17);
	PS.gridColor(COLOR_WALL);

	PS.statusText("Snake Box");
	PS.statusColor(COLOR_BLANK);

	PS.color(PS.ALL, PS.ALL, COLOR_BLANK);
	PS.border(PS.ALL, PS.ALL, 0);

	snake = new Snake();

	PS.timerStart(10, function () {
		snake.move();
	});
};

PS.touch = function (x, y, data, options) {
	"use strict";

	if (PS.color(x, y) === COLOR_WALL) {
		PS.color(x, y, COLOR_BLANK);
	} else if (PS.color(x, y) === COLOR_BLANK) {
		PS.color(x, y, COLOR_WALL);
	}
};

var wrapPos = function(pos) {
	var result = {
		x: pos.x % PS.gridSize().width,
		y: pos.y % PS.gridSize().height
	};
	if (result.x < 0) {
		result.x += PS.gridSize().width;
	}
	if (result.y < 0) {
		result.y += PS.gridSize().height;
	}
	return result;
}

var turnLeft = function(dir) {
	if (Math.abs(dir.x) > Math.abs(dir.y)) {
		return ({
			x: 0,
			y: -dir.x
		});
	} else {
		return ({
			x: dir.y,
			y: 0
		});
	}
}

var turnRight = function(dir) {
	if (Math.abs(dir.x) > Math.abs(dir.y)) {
		return ({
			x: 0,
			y: dir.x
		});
	} else {
		return ({
			x: -dir.y,
			y: 0
		});
	}
}

var Snake = function Snake() {
	this.pos = { x: PS.gridSize().width / 2, y: PS.gridSize().height / 2 };
	this.dir = { x: -1, y: 0 };
	this.length = 5;
	this.body = [];

	for (var i = 0; i < this.length; i++) {
		var pos = { x: this.pos.x - (this.dir.x * i), y: this.pos.y - (this.dir.y * i) };
		this.body.push(pos);
		PS.color(pos.x, pos.y, COLOR_SNAKE);
	}

	this.inFront = function () {
		return wrapPos({
			x: this.body[0].x + this.dir.x,
			y: this.body[0].y + this.dir.y
		});
	};

	this.toLeft = function () {
		return wrapPos({
			x: this.body[0].x + turnLeft(this.dir).x,
			y: this.body[0].y + turnLeft(this.dir).y
		});
	};

	this.toRight = function () {
		return wrapPos({
			x: this.body[0].x + turnRight(this.dir).x,
			y: this.body[0].y + turnRight(this.dir).y
		});
	};

	this.isBeadSolid = function (pos) {
		return PS.color(pos.x, pos.y) === COLOR_WALL
	};

	this.handleCollisions = function () {
		if (this.isBeadSolid(this.inFront())) {
			if (this.isBeadSolid(this.toRight()) && this.isBeadSolid(this.toLeft())) {
				return false;
			}
			
			if (this.isBeadSolid(this.toRight())) {
				this.dir = turnLeft(this.dir);
				return true;
			}
			if (this.isBeadSolid(this.toLeft())) {
				this.dir = turnRight(this.dir);
				return true;
			}

			if (PS.random(1) > 0.5) {
				this.dir = turnLeft(this.dir);
			} else {
				this.dir = turnRight(this.dir);
			}
		}
		return true;
	};

	this.move = function () {
		if (!this.handleCollisions()) return;

		PS.color(this.body[this.length - 1].x, this.body[this.length - 1].y, COLOR_BLANK);

		for (var i = this.length - 1; i > 0; i--) {
			this.body[i].x = this.body[i - 1].x;
			this.body[i].y = this.body[i - 1].y;
		}

		this.body[0] = this.inFront();

		PS.color(this.body[0].x, this.body[0].y, COLOR_SNAKE);
	};
};